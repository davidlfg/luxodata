<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

 /**
 * Preprocess node template variables.
 */
function luxodata_preprocess_node(&$variables) {
  // Variables for node--services.tpl.php
  if ($variables['type'] == 'services') {
    $node = $variables['node'];
    $variables['service_imagen'] = render($variables['content']['field_image']);
    $variables['service_service'] = render($variables['content']['field_services']);
    $variables['service_content'] = render($variables['content']['body']);
    $variables['service_technical'] = fn_luxodata_regulation_markup($variables['content']['field_services_regulations']['#items']);
  }
  if ($variables['type'] == 'article') {
    $variables['title_suffix'] = '<div id="title-suffix">' . $variables['title'] . '</div>';
  }
}

/**
 * Function markup reglamentos tecnicos()
 */
function fn_luxodata_regulation_markup($items) {
	$regulation = array();
	$ids = array();
	$output = '';
	foreach ($items as $key => $value) {
  $node = node_load($value['target_id']);
  $variables = array(
      'path' => $node->field_image['und'][0]['uri'], 
      'alt' => 'Test alt',
      'title' => $node->title,
      'width' => '85',
      'height' => '35',
      );
  $output .= '<div class="image-"'. $value['target_id'] .'>'.theme('image', $variables).'</div>';
	}
	return $output;
}