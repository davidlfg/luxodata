<?php
/**
 * @file
 * Template to render campaign nodes.
 */
if ($node->type == "services"): ?>
  <div class="services-type-<?php print $node->type;?>">
    <div class="service-zone-uno"><?php print $service_service; ?></div>
    <div class="service-zone-dos">
      <div class="service-column-uno"><?php print $service_imagen; ?></div>
      <div class="service-column-dos body">
        <div class="service-empresa"></div>
        <div class="title"><?php print $title; ?></div>
        <div class="service-body"><?php print $service_content; ?></div>
        <div class="service-technical"><div class="label">Reglamentos Técnicos</div><div class="images"><?php print $service_technical; ?></div></div>
      </div>
    </div>
  </div>
<?php endif;