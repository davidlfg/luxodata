(function ($) {
  Drupal.behaviors.script = {
    attach: function (context, settings) {
      // Window width
      var initial_window = $(window).width();
      var width_calcule =  initial_window / 9;
      $('.view-experience .views-row').css('width', width_calcule);
      $(window).resize(function() {
        var  resize_window = $(window).width();
        var resize_calcule = resize_window / 9;
      $('.view-experience .views-row').css('width', resize_calcule);
      });
      //ScrollTop
      $('.view-menu .views-row a').click(function(){        
      	var seccion = $(this).attr('href');
        if ($('body').hasClass('not-front')) {
          window.top.location.href = window.location.origin+'/'+seccion;
        } else {
          $('body, html').animate({
            scrollTop: $(seccion).offset().top-122
          }, 800)
          return false;
        }  	
      });
      //
      //   var secciones = [$('#region-nosotros').offset().top,$('#region-services').offset().top,$('#region-model-services').offset().top,$('#block-views-experience-experiencie').offset().top,$('#zone-social').offset().top] 
      //   console.log(secciones[0]);

      //   if($(this).scrollTop() > secciones[0]-122 && $(this).scrollTop() < secciones[1]-122){
      //     console.log('hola');
      //   }
      // }
      //Menu movil
      $('.region-menu .block-title').click(function(){
        $('.region-menu .view-menu').toggle(1000);
      });
      $('.view-menu a').click(function(){
        if( initial_window < 740){
          $('.region-menu .view-menu').hide();
        }
      })
      $(window).resize(function(){
        if( initial_window < 740){
          $('.region-menu .view-menu').hide();
        }
      });
      //Nuestros servicios
      $('.ui-accordion h3.ui-accordion-header').click(function(){
        var bg_ser = $('.view-services .ui-accordion-header.ui-accordion-header-active').css('background-color');
        $('.view-services').css('background', bg_ser);
      });
      //If article
      if ($('body').hasClass('node-type-article')) {
        var title = $('#title-suffix');
        $('.field-name-field-image .field-item').append($(title));
        //Cambiar divs
        $('#zone-articles').append($('#zone-subanertwo'));
      }
      //Ocultar labels de formularios
      if ($('.block-webform div').hasClass('error')) {
        hide_label_by_selector('.webform-client-form .form-item#webform-component-webform-name', 'input');
        hide_label_by_selector('.webform-client-form .webform-component-email','input');
        hide_label_by_selector('.webform-client-form #webform-component-webform-subject','input');
        hide_label_by_selector('.webform-client-form .form-item.webform-component-textarea','textarea');
      }

      hide_label_newsletter();
      hide_labels_webform();
    }
  }

  function hide_label_by_selector(selector, input) {
    if($(selector+' '+input).val() != '')
      $(selector+' label').hide();
  }

  //Ocultar los labels en los formularios NEWSLETTER
  function hide_label_newsletter() {
    $('.input-newsletter input[type="text"]').click(function() {
      $('.input-newsletter span').hide();
    });
    $('.input-newsletter span').click(function() {
      $('.input-newsletter span').hide();
    });
    $('.input-newsletter input[type="text"]').focusout(function() {
      if ($(this).val() == '')
        $('.input-newsletter span').show();
    });
  }

  //Ocultar los labels del formulario WEBFORM
  function hide_labels_webform() {
    $('#webform-client-form-1 .form-item').click(function() {
      $(this).find('label').hide();
    });
    $('#webform-client-form-1 .form-item').focusout(function() {
      if ($(this).find('input').val() == '' || $(this).find('textarea').val() == '')
        $(this).find('label').show();
    });
  }

})(jQuery);